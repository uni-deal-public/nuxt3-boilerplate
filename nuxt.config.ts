// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    modules: [
        "@unocss/nuxt"
    ],
    css: ['~/assets/app.css'],
    devtools: {
        enabled: true,
        timeline: {
            enabled: true
        }
    },
    features: {
        // noScripts: false, // Default false
    },
    experimental: {
        // externalVue: false, // Default true
        //
        // asyncEntry: true, // Default false
        headNext: true, // Default false
        //
        // renderJsonPayloads: false, // Default true
        // payloadExtraction: false, // Default true
        //
        // writeEarlyHints: true // Default false
    }
})
