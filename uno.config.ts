import {
    defineConfig,
    presetWebFonts,
    presetIcons,
    presetWind,
    transformerDirectives,
    transformerCompileClass,
    transformerVariantGroup,
} from 'unocss'
import {FileSystemIconLoader} from "@iconify/utils/lib/loader/node-loaders";


export default defineConfig({

    presets: [
        presetWebFonts({
            provider: 'bunny',
            fonts: {
                roboto: [{
                    name: 'Roboto',
                    weights: [300, 400, 500, 600]
                }, 'sans-serif'],

                "roboto-slab": [{
                    name: 'Roboto Slab',
                    weights: [300, 400, 500]
                }, 'sans-serif']
            }
        }),
        presetIcons({
            warn: true,
            extraProperties: {
                display: 'inline-block',
                'vertical-align': 'middle',
            },
            collections: {
                lal: FileSystemIconLoader(
                    './assets/icons',
                    svg => svg.replace(/^<svg /, '<svg fill="currentColor" ')
                )
            }
        }),
        presetWind(),
    ],
    transformers: [
        transformerDirectives(),
        transformerCompileClass({
            classPrefix: '',
        }),
        transformerVariantGroup(),
    ],
})
