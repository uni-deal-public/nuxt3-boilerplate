import type {UseSeoMetaInput} from "@unhead/schema";
import type {UseHeadOptions} from "@unhead/vue";
import {withBase, withoutTrailingSlash} from "ufo";

export type UseSeoInput = Omit<UseSeoMetaInput, "title"> & {
    title?: string
    canonical?: string
    image?: string
}
export default function useSeo(input: UseSeoInput, options?: UseHeadOptions, onlyOnServer?: boolean) {

    // Add social title if missing
    if (input.title) {
        input.twitterTitle ??= input.title;
        input.ogTitle ??= input.title;
    }

    // Define image if needed
    if (input.image) {
        input.twitterImage ??= input.image;
        input.ogImage ??= input.image;
        delete input.image
    }

    // Add social description if missing
    if (input.description) {
        input.twitterDescription = input.description;
        input.ogDescription ??= input.description;
    }

    // Set twitterCard default
    input.twitterCard ??= 'summary_large_image';


    // Processing canonical URL

    input.canonical ??= useRoute().fullPath;
    input.canonical = withBase(input.canonical, useAppConfig().siteUrl)
    input.canonical = withoutTrailingSlash(input.canonical, true)

    input.ogUrl ??= input.canonical


    if (!onlyOnServer) {
        useHead({link: [{rel: 'canonical', href: input.canonical}]})
        delete input.canonical;
        useSeoMeta(input, options)
    } else {
        useServerHead({link: [{rel: 'canonical', href: input.canonical}]})
        delete input.canonical;
        useServerSeoMeta(input, options)
    }
}
